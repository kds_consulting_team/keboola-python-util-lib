import os
import unittest
from datetime import datetime

from freezegun import freeze_time

from kbc import env_handler


class TestEnvHandler(unittest.TestCase):

    def setUp(self):
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            'data')
        os.environ["KBC_DATADIR"] = path

    def test_empty_required_params_pass(self):
        # set env
        hdlr = env_handler.KBCEnvHandler(mandatory_params=[])

        # tests
        try:
            hdlr.validate_config()
        except Exception:  # noeq
            self.fail("validateConfig() fails on empty Parameters!")

    def test_required_params_missing_fail(self):
        # set env - missing notbar
        hdlr = env_handler.KBCEnvHandler(mandatory_params=['fooBar', 'notbar'])

        with self.assertRaises(ValueError) as er:
            hdlr.validate_config(['fooBar', 'notbar'])

        self.assertEqual('Missing mandatory config parameters fields: [notbar] ', str(er.exception))

    # TEST backfill mode function
    @freeze_time("2010-10-10")
    def test_backfill_mode_period_no_state_first_step(self):
        # setup
        hdlr = env_handler.KBCEnvHandler(mandatory_params=['fooBar', 'notbar'])
        # override config params
        start_date = '2010-09-01'
        end_date = '2010-10-10'
        # expected res
        res_start = datetime(2010, 9, 1)
        res_end = datetime(2010, 10, 10)

        start_date, end_date = hdlr.get_backfill_period(start_date, end_date, None)
        self.assertEqual(start_date, res_start)
        self.assertEqual(res_end, res_end)

    @freeze_time("2010-10-10")
    def test_backfill_mode_period_state_last_step(self):
        # setup
        last_state = {"last_period": {"start_date": "2010-10-05", "end_date": "2010-10-10"}}
        hdlr = env_handler.KBCEnvHandler(mandatory_params=['fooBar', 'notbar'])
        # override config params
        start_date = '2010-09-01'
        end_date = '2010-10-10'
        # expected res
        res_start = datetime(2010, 10, 10)
        res_end = datetime(2010, 10, 10)

        start_date, end_date = hdlr.get_backfill_period(start_date, end_date, last_state)
        self.assertEqual(start_date, res_start)
        self.assertEqual(res_end, res_end)

    @freeze_time("2010-10-10")
    def test_backfill_mode_period_state_last_intermediate_step_full(self):
        # setup
        last_state = {"last_period": {"start_date": "2010-10-01", "end_date": "2010-10-05"}}
        hdlr = env_handler.KBCEnvHandler(mandatory_params=['fooBar', 'notbar'])
        # override config params / 4 day chunks
        start_date = '2010-09-01'
        end_date = '2010-09-04'
        # expected res - end_date + 4 days
        res_start = datetime(2010, 10, 5)
        res_end = datetime(2010, 10, 9)

        start_date, end_date = hdlr.get_backfill_period(start_date, end_date, last_state)
        self.assertEqual(start_date, res_start)
        self.assertEqual(res_end, res_end)

    @freeze_time("2010-10-10")
    def test_backfill_mode_period_state_last_intermediate_step_part(self):
        # setup
        last_state = {"last_period": {"start_date": "2010-10-05", "end_date": "2010-10-09"}}
        hdlr = env_handler.KBCEnvHandler(mandatory_params=['fooBar', 'notbar'])
        # override config params / 4 day chunks
        start_date = '2010-09-01'
        end_date = '2010-09-04'
        # expected res - end_date + 4 days
        # expected res - end_date + 1 days
        res_start = datetime(2010, 10, 9)
        res_end = datetime(2010, 10, 10)

        start_date, end_date = hdlr.get_backfill_period(start_date, end_date, last_state)
        self.assertEqual(start_date, res_start)
        self.assertEqual(res_end, res_end)

    """
    def test_remove_header_and_delete(self):
        # set env
        hdlr = env_handler.KBCEnvHandler(mandatory_params=[])
        
        # Create a temporary directory
        test_dir = tempfile.mkdtemp()
    """


if __name__ == '__main__':
    unittest.main()
