from distutils.core import setup
import setuptools

setup(
    name='kbc',
    version='0.5.1',
    setup_requires=['setuptools_scm'],
    url='https://bitbucket.org/kds_consulting_team/keboola-python-util-lib',
    download_url='https://bitbucket.org/kds_consulting_team/keboola-python-util-lib',
    packages=setuptools.find_packages(),
    install_requires=[
        'pytz',
        'python-dateutil',
        'pygelf',
        'requests',
        'dateparser',
        'keboola @ https://github.com/keboola/python-docker-application/zipball/2.1.1#egg=keboola-2.1.1',
        'deprecated'
    ],
    test_suite='tests',
    license="MIT"
)
