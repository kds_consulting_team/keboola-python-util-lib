FROM python:3.7.2-alpine
ENV PYTHONIOENCODING utf-8

COPY . /code/
RUN apk add --no-cache --virtual .build-deps gcc musl-dev
RUN apk add --update tzdata
ENV TZ=UTC
RUN pip install flake8

RUN pip install -r /code/requirements.txt

WORKDIR /code/


CMD ["python", "-u", "/code/component.py"]
