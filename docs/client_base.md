# kbc.client_base

## HttpClientBase
```python
HttpClientBase(self, base_url, max_retries=10, backoff_factor=0.3, status_forcelist=(500, 502, 504), default_http_header=[], auth=None, default_params=None)
```

Base class for implementing a single Http client related to some REST API.


**Args:**

- `base_url:` The base URL for this endpoint. e.g. `https://exampleservice.com/api_v1/`    
- `max_retries:` Total number of retries to allow.
    
- `backoff_factor`:  A back-off factor to apply between attempts.    
- `status_forcelist`:  A set of HTTP status codes that we should force a retry on. e.g. `[500,502]`
- `default_http_header` (dict): Default header to be sent with each request
                    eg. 
                    ```json
                    {'Authorization': 'Bearer ' + token,
                            'Content-Type' : 'application/json',
                            'Accept' : 'application/json'}
                    ```
- `auth`: Default Authentication tuple or object to attach to (from  requests.Session().auth).
                  eg. auth = (user, password)
- `default_params` (dict): default parameters to be sent with each request eg. `{'param':'value'}`

### get_raw
```python
HttpClientBase.get_raw(self, url, params={}, **kwargs)
```

Construct a requests GET call with args and kwargs and process the
results.


Args:
    url (str): requested url
    params (dict): additional url params to be passed to the underlying
        requests.get
    **kwargs: Key word arguments to pass to the get requests.get

Returns:
    r (requests.Response): :class:`Response <Response>` object.

Raises:
    requests.HTTPError: If the API request fails.

### post_raw
```python
HttpClientBase.post_raw(self, *args, **kwargs)
```

Construct a requests POST call with args and kwargs and process the
results.

Args:
    *args: Positional arguments to pass to the post request.
       Accepts supported params in requests.sessions.Session#request
    **kwargs: Key word arguments to pass to the post request.
       Accepts supported params in requests.sessions.Session#request
       eg. params = {'locId':'1'}, header = {some additional header}
       parameters and headers are appended to the default ones

Returns:
    Response: Returns :class:`Response <Response>` object.

Raises:
    requests.HTTPError: If the API request fails.

### post
```python
HttpClientBase.post(self, *args, **kwargs)
```

Construct a requests POST call with args and kwargs and process the
results.

Args:
    *args: Positional arguments to pass to the post request.
       Accepts supported params in requests.sessions.Session#request
    **kwargs: Key word arguments to pass to the post request.
       Accepts supported params in requests.sessions.Session#request
       eg. params = {'locId':'1'}, header = {some additional header}
       parameters and headers are appended to the default ones


Returns:
    body: json reposonse json-encoded content of a response

Raises:
    requests.HTTPError: If the API request fails.

