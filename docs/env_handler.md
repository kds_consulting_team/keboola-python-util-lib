# kbc

# kbc.env_handler

## KBCEnvHandler
```python
KBCEnvHandler(self, mandatory_params, data_path=None)
```

Class handling standard tasks for KBC component manipulation i.e. config load, validation

It contains some useful methods helping with boilerplate tasks.

### validate_config
```python
KBCEnvHandler.validate_config(self, mandatory_params)
```

Validates config parameters based on provided mandatory parameters.
All provided parameters must be present in config to pass.

**ex1.:**
```python
par1 = 'par1'
par2 = 'par2'
mandatory_params = [par1, par2]
KBCEnvHandler.validate_config(mandatory_params)
```
Validation will fail when one of the above parameters is not found.

**Two levels of nesting:**

Parameters can be grouped as arrays: `par3 = [groupPar1, groupPar2]`
=> at least one of the pars has to be present

**ex2.**
```python
par1 = 'par1'
par2 = 'par2'
par3 = 'par3'
groupPar1 = 'groupPar1'
groupPar2 = 'groupPar2'
group1 = [groupPar1, groupPar2]
group3 = [par3, group1]
mandatory_params = [par1, par2, group1]KBCEnvHandler.validate_config(mandatory_params)

```
Folowing logical expression is evaluated:
`par1 AND par2 AND (groupPar1 OR groupPar2)`

**ex3**
```python
par1 = 'par1'
par2 = 'par2'
par3 = 'par3'
groupPar1 = 'groupPar1'
groupPar2 = 'groupPar2'
group1 = [groupPar1, groupPar2]
group3 = [par3, group1]
mandatory_params = [par1, par2, group3]
mandatory_params = [par1, par2, group1]KBCEnvHandler.validate_config(mandatory_params)
```
Following logical expression is evaluated:
`par1 AND par2 AND (par3 OR (groupPar1 AND groupPar2))`

### validate_image_parameters
```python
KBCEnvHandler.validate_image_parameters(self, mandatory_params)
```

Validates `image_parameters` section based on provided mandatory parameters.
All provided parameters must be present in config to pass.

for details [see validate_config](###validate_config)

### set_default_logger
```python
KBCEnvHandler.set_default_logger(self, log_level='INFO')
```

Sets default console logger.

Args:
    log_level: logging level, default: 'INFO'

Returns: logging object


### get_state_file
```python
KBCEnvHandler.get_state_file(self)
```


Return dict representation of state file or nothing if not present

Returns:
    dict:


### write_state_file
```python
KBCEnvHandler.write_state_file(self, state_dict)
```

Stores state file.
Args:
    state_dict:

### create_sliced_tables
```python
KBCEnvHandler.create_sliced_tables(self, folder_name, pkey=None, incremental=False, src_delimiter=',', src_enclosure='"', dest_bucket=None)
```

Creates prepares sliced tables from all files in DATA_PATH/out/tables/{folder_name} - i.e. removes all headers
and creates single manifest file based on provided parameters.

**Args:**

- folder_name: folder name present in DATA_PATH directory that contains files for slices, the same name will be used as table name
- `pkey`: array of pkeys
- incremental: boolean
- src_delimiter: delimiter of the source file [,]
- src_enclosure: enclosure of the source file ["]
- dest_bucket: name of the destination bucket, eg. in.c-input (optional)


### create_manifests
```python
KBCEnvHandler.create_manifests(self, results:List[kbc.result.KBCResult], headless=False, incremen
tal=True)
```

Write manifest files for the results produced by kbc.results.ResultWriter
:param results: List of result objects
:param headless: Flag whether results contain sliced headless tables and hence the `.column` attr
ibute should be
used in manifest file.
:param incremental:
:return:


### get_and_remove_headers_in_all
```python
KBCEnvHandler.get_and_remove_headers_in_all(self, files, delimiter, enclosure)
```

Removes header from all specified files and return it as a list of strings

Throws error if there is some file with different header.


### get_past_date
```python
KBCEnvHandler.get_past_date(self, str_days_ago, to_date=None, tz=<UTC>)
```

```python
def get_past_date(self, str_days_ago: str, to_date: datetime = None, 
tz: pytz.tzinfo.BaseTzInfo = pytz.utc) -> object:
```
Returns date in specified timezone relative to today.


e.g.
`'5 hours ago'`,
`'yesterday'`,
`'3 days ago'`,
`'4 months ago'`,
`'2 years ago'`,
`'today'`

### split_dates_to_chunks
```python
KBCEnvHandler.split_dates_to_chunks(self, start_date, end_date, intv, strformat='%m%d%Y')
```

Splits dates in given period into chunks of specified max size.
 
**Params:**
start_date -- start_period [datetime]
end_date -- end_period [datetime]
intv -- max chunk size
strformat -- dateformat of result periods

**Usage example:**
```python
list(split_dates_to_chunks("2018-01-01", "2018-01-04", 2, "%Y-%m-%d"))
```
returns 
```python
[{start_date: "2018-01-01", "end_date":"2018-01-02"}
 {start_date: "2018-01-02", "end_date":"2018-01-04"}]
 ```

