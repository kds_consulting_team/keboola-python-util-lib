# kbc.csv_tools

## CachedOrthogonalDictWriter
```python
CachedOrthogonalDictWriter(file_path, fieldnames, temp_directory=None, dialect="excel", *args, **kwds)
```

DictWriter, built on top of csv.DictWriter, that supports automatic extension of headers according to what data it receives.
The result file always has a complete header (as defined by fieldnames) or it is extended if some new columns
 are introduced in the data. It always produces a valid CSV (missing columns are filled with blanks).
 It uses a series of cached writers / files that are merged into a single one with final set of columns on close()

**NOTE:** If not using "with" statement, close() method must be called at the end of processing to get the result.

**NOTE:** The final column list is stored in `fieldnames` property:

```python
columns = writer.fieldnames
```

**NOTE:** Does not keep the order of rows added - the rows containing additional headers always come first:

### Example:

```python
from kbc.csv_tools import CachedOrthogonalDictWriter
file = '/test/test.csv'
wr = CachedOrthogonalDictWriter(file, ["a", "b" , "c"])
wr.writerow({"a":1,"b":2})
wr.writerow({"b":2, "d":4})
wr.close()
```

leads to CSV with following content:
   
|a  |b  |c  |d  |
|---|---|---|---|
|   |2  |   |4  |
|1  | 2 |   |   |

May be also used with `with` statement to automatically close once finished:

```python
from kbc.csv_tools import CachedOrthogonalDictWriter
file = '/test/test.csv'
with CachedOrthogonalDictWriter(file, ["a", "b" , "c"]) as wr:
    wr.writerow({"a":1,"b":2})
    wr.writerow({"b":2, "d":4})

# get final headers
final_header = wr.fieldnames
```

:param file_path: result file path
:param fieldnames: Minimal column list
:param temp_directory: Optional path to a temp directory for cached files. By default the same directory as the
output file is used. Temporary files/directory are deleted on close.
:param dialect: As in csv package
:param args: As in csv package
:param kwds: As in csv package

### writerow

```python
CachedOrthogonalDictWriter.writerow(self, row_dict: dict)
```
### writerows

```python
CachedOrthogonalDictWriter.writerows(self, row_dict: List[dict])
```
### close

```python
CachedOrthogonalDictWriter.close(self)
```
Close all output streams / files and build and move the final result file.
Has to be called before result processing.
