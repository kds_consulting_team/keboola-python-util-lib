# kbc.result

## ResultWriter
```python
ResultWriter(self, result_dir_path:str, table_def:kbc.result.KBCTableDef, fix_headers=False, exclude_fields=[], user_value_cols=[], flatten_objects=True, buffer_size=8192)
```

Writer for parsing and storing JSON responses. It allows basic flattening of json object and handling of Array objects.

The Writer object support `_enter_` and `_close_` methods so it can be used in `with` statement. Otherwise method
`close()` has to be called before result retrieval. The write method opens output stream which is kept during the whole
lifetime of an instance, until the `close()` or `_close_` method is called (`with` statement exited)

### write
```python
ResultWriter.write(self, data, file_name=None, user_values=None, object_from_arrays=False, write_header=True)
```

Write the Json result using the writer configuration. Keeps the output stream opened and every result is written to
the resulting file assuring the memory efficiency.

:param data:
:param file_name:
:param user_values: Optional dictionary of user values to be added to the root object. The keys must match the column
names specified during initialization in `user_value_cols` parameter, otherwise it will throw a ValueError.

If used with `fix_headers` option, `user_value` columns must be specified on the table def object.

:param object_from_arrays: Create additional tables from the array objects
The result tables are named like: `parent_array-colnam.csv' and generates PK values: [parent_key, row_number]

:param write_header: Optional flag specifying whether to write the header - only needed when creating sliced files.
:return:

### collect_results
```python
ResultWriter.collect_results(self)
```

Collect the results.

NOTE: If not called after a `with` statement, method `close()` needs to be called prior processing
any of the result files to close all the streams.

:return: List of KBCResult objects

### close
```python
ResultWriter.close(self)
```

Close all output streams / files. Has to be called at end of extraction, before result processing.

:return:

