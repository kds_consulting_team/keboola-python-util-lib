import io
import os
from dataclasses import dataclass
from typing import List, Dict

from kbc.csv_tools import CachedOrthogonalDictWriter

"""
Set of classes for standardized result processing, without use of Pandas library - more memory efficient.

Defines three classes
- KBCResult - definition of a result that can be processed by other methods
- KBCTableDef - definition of a KBC Storage table - column, pkeys, name and other metadata
- ResultWriter - Class providing methods of generic parsing and writing JSON responses.
Can be used to build more complex
 writer based on this class as a parent.

"""


# TODO: Support for sliced tables. Flag on KBC result and modified full_path to contain just folder

@dataclass
class KBCTableDef:
    pk: List[str]
    columns: List[str]
    name: str
    destination: str
    metadata: Dict = None
    column_metadata: Dict = None

    def __getitem__(self, index):
        return getattr(self, index)


@dataclass
class KBCResult:
    file_name: str
    full_path: str
    table_def: KBCTableDef
    incremental: bool = False

    def __getitem__(self, index):
        return getattr(self, index)


class ResultWriter:
    """
    Writer for parsing and storing JSON responses. It allows basic flattening of json object a
    nd handling of Array objects.

    The Writer object support `_enter_` and `_close_` methods so it can be used in `with` statement. Otherwise method
    `close()` has to be called before result retrieval. The write method opens output stream which is kept during
    the whole lifetime of an instance, until the `close()` or `_close_` method is called (`with` statement exited)
    """

    def __init__(self, result_dir_path: str, table_def: KBCTableDef, fix_headers=False,
                 exclude_fields=None, user_value_cols=None, flatten_objects=True, child_separator: str = '.',
                 buffer_size=io.DEFAULT_BUFFER_SIZE):
        """

         :type child_separator: String that will separate child objects while flattening
         :param write_header: asddas
         :param write_header: object
         :param table_def: KBCTableDef
         :param result_dir_path: str
         :param fix_headers: flag whether to use minimal header specified in table definition
         or rather infer the header from the result itself.
         Columns need to be specified in the TableDef object. If set to true, result will
         always contain at least the specified header.

         :param flatten_objects: Specifies whether to flatten the parsed object or write them as are.
         If set to true all object hierarchies are flattened with `_` separator. Arrays are not parsed and stored as
         normal values.

         **Note:** If set to `True` and  used in combination with `fix_headers` field the columns specified include
         all flattened objects, hierarchy separated by `_` e.g. cols = ['single_col', 'parent_child'] otherwise these
         will not be included.

         :param exclude_fields: List of col names to be excluded from parsing.
         Usefull with `flatten_object` parameter on. This is applied before flattening.

         :param user_value_cols: List of column names that will be added to the root object. Use this in cocnlusion
         with `user_values` parameter in the write() method.

         :param buffer_size: Optional size of buffer used with Writer object.
         :type buffer_size: int

         :rtype: object
         """
        self.result_dir_path = result_dir_path
        self.res_file_path = os.path.join(self.result_dir_path, table_def.name + '.csv')
        self.table_def = table_def
        if user_value_cols:
            self.table_def.columns += user_value_cols
        self.results = {}
        self.fix_headers = fix_headers
        self.exclude_fields = exclude_fields if exclude_fields else []
        self.flatten_objects = flatten_objects
        self.child_separator = child_separator
        self.user_value_cols = user_value_cols if user_value_cols else []

        # Cache open files, to limit expensive open operation count
        self._csv_writer_cache = {}
        self._buffer_size = buffer_size

    def write_all(self, data_array, file_name=None, user_values=None, object_from_arrays=False, write_header=True):

        for data in data_array:
            self.write(data, file_name=file_name, user_values=user_values, object_from_arrays=object_from_arrays,
                       write_header=write_header)

    def write(self, data, file_name=None, user_values=None, object_from_arrays=False, write_header=True):
        """
        Write the Json result using the writer configuration. Keeps the output stream opened and every result is
        written to the resulting file assuring the memory efficiency.

        :param data:
        :param file_name:
        :param user_values: Optional dictionary of user values to be added to the root object. The keys must match
        the column names specified during initialization in `user_value_cols` parameter,
        otherwise it will throw a ValueError.

        If used with `fix_headers` option, `user_value` columns must be specified on the table def object.

        :param object_from_arrays: Create additional tables from the array objects
        The result tables are named like: `parent_array-colnam.csv' and generates PK values: [parent_key, row_number]

        :param write_header: Optional flag specifying whether to write the header
        - only needed when creating sliced files.
        :return:
        """
        if not data:
            return {}

        if file_name:
            self.res_file_path = os.path.join(self.result_dir_path, file_name)

        # exclude columns
        data = self._exclude_cols(data)

        # flatten objects
        if self.flatten_objects:
            data = self.flatten_json(data)

        # add user values
        data = self._add_user_values(data, user_values)

        # write array objects
        if object_from_arrays:
            res = self._write_array_object(data, write_header, user_values)
            self.results = {**self.results, **res}

        fieldnames = self._get_header(data)
        # update columns in result if not specified and sort (dictwriter depends on column list order)
        if not self.fix_headers:
            fieldnames.sort()
            self.table_def.columns = fieldnames

        self._write_data_to_csv(data, self.res_file_path, write_header, fieldnames)

        self.results[file_name] = (KBCResult(file_name, self.res_file_path, self.table_def))

    def collect_results(self):
        """
        Collect the results.

        NOTE: If not called after a `with` statement, method `close()` needs to be called prior processing
        any of the result files to close all the streams.

        :return: List of KBCResult objects
        """

        return [self.results[r] for r in self.results]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        """
        Close all output streams / files. Has to be called at end of extraction, before result processing.

        :return:
        """
        for res in self._csv_writer_cache:
            self._csv_writer_cache[res].close()

        # set final columns
        if self._csv_writer_cache.get(self.res_file_path):
            self.table_def.columns = self._csv_writer_cache[self.res_file_path].fieldnames

    def _write_array_object(self, data, write_header, user_values):
        results = {}
        for arr in [(key, data[key]) for key in data.keys() if isinstance(data[key], list)]:
            res = [{"row_nr": idx, "parent_key": self._get_pkey_values(data, user_values), "value": val}
                   for idx, val in enumerate(arr[1])]
            tb_name = self.table_def.name + '_' + arr[0]
            filename = tb_name + '.csv'
            res_path = os.path.join(self.result_dir_path, filename)
            columns = ["row_nr", "parent_key", "value"]
            self._write_data_to_csv(res, res_path, write_header, columns)
            res = KBCResult(filename, res_path,
                            KBCTableDef(["row_nr", "parent_key"], columns, tb_name, ''))
            results[res.file_name] = res
            # remove field from source
            data.pop(arr[0])
        return results

    def _get_pkey_values(self, data, user_values):
        pkeys = []
        for k in self.table_def.pk:
            if data.get(k):
                pkeys.append(str(data[k]))
            elif user_values.get(k):
                pkeys.append(str(user_values[k]))

        return '|'.join(pkeys)

    def _write_data_to_csv(self, data, res_file_path, write_header, fieldnames):
        # append if exists
        if self._csv_writer_cache.get(res_file_path):
            writer = self._csv_writer_cache.get(res_file_path)
            write_header = False
        else:
            writer = CachedOrthogonalDictWriter(res_file_path, fieldnames, buffering=self._buffer_size,
                                                temp_directory=f'{res_file_path}_temp')
            self._csv_writer_cache[res_file_path] = writer

        # not needed since OrthogonalWriter is being used
        # # fix headers
        # data = self._fix_fieldnames(data, fieldnames)

        if write_header:
            writer.writeheader()
        if isinstance(data, list):
            writer.writerows(data)
        else:
            writer.writerow(data)

    def _get_header(self, data):
        if self.fix_headers:
            cols = self.table_def.columns
        else:
            cols = list(data.keys())
        cols.sort()
        return cols

    def flatten_json(self, x, out=None, name=''):
        if out is None:
            out = dict()
        if type(x) is dict:
            for a in x:
                self.flatten_json(x[a], out, name + a + self.child_separator)
        else:
            out[name[:-len(self.child_separator)]] = x

        return out

    def _exclude_cols(self, data):
        for col in self.exclude_fields:
            if col in data.keys():
                data.pop(col)
        return data

    def _add_user_values(self, data, user_values):
        if not user_values:
            return data

        # validate
        if len(user_values) > 0 and not all(elem in user_values.keys() for elem in self.user_value_cols):
            raise ValueError("Some user value keys (%s) were not set in user_value_cols (%s) during initialization!",
                             user_values.keys(), self.user_value_cols)

        data = {**data, **user_values}

        for key in user_values:
            data[key] = user_values[key]
        return data

    def _fix_fieldnames(self, data, column_names):
        """
        Fix columns in the dictionary. Adds empty ones if key not present
        :param data:
        :return:
        """
        if isinstance(data, list):
            result = []
            for row in data:
                result.append(self._fix_fieldnames(row, column_names))
        else:
            result = {}
            for key in column_names:
                result[key] = data.get(key, '')

        return result
