**0.0.3**

- src folder structure
- remove dependency on handler lib - import the code directly to modify

**0.0.2**

- add dependency to base lib
- basic tests

**0.0.1**

- add utils scripts
- move kbc tests directly to pipelines file
- use uptodate base docker image
- add changelog
